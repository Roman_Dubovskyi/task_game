package general.controller;

/**
 * This interface creates logic of a controller.
 */
public interface Controller {
  /**
   * This method starts single game mode.
   */
  void executeSingleGame();

  /**
   * This method prints set of rules for tournament.
   */
  void printTournamentRules();

  /**
   * This method prints set of rules for single game.
   */
  void printSingleGameRules();

  /**
   * This method starts tournament.
   */
  void makeTournament();
}
