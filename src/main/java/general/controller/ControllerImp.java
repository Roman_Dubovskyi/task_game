package general.controller;

import general.model.singleMod.SingleMode;
import general.model.tournament.Tournament;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This class implements logic of controller.
 */
public class ControllerImp implements Controller {
  private SingleMode series = new SingleMode();
  private Tournament tour = new Tournament();

  public void executeSingleGame() {
    series.playSeries();
  }

  public void makeTournament() {
    tour.playTournament();
  }

  public void printSingleGameRules() {
    try {
      String contents = new String(Files.readAllBytes(Paths.get("singleGameRules.txt")));
      System.out.println(contents);
    } catch (IOException e) {
      System.out.println("problem with the file");
    }
  }

  public void printTournamentRules() {
    try {
      String contents = new String(Files.readAllBytes(Paths.get("tournamentRules.txt")));
      System.out.println(contents);
    } catch (IOException e) {
      System.out.println("problem with the file");
    }
  }
}
