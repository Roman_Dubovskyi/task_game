package general.model.singleMod;

import general.model.ModelOfGame;
import general.model.player.Player;

import java.util.Scanner;

/**
 * This class implements interface 'ModelOfGame' and
 * is responsible for creating a logic of whole series
 * of BlackJack.
 */

public class SingleMode implements ModelOfGame {
  private Game game = new Game();
  private Player user;
  private Player dealer;
  private int bet;
  private Scanner input = new Scanner(System.in, "UTF-8");

  @Override
  public void playSeries() {
    System.out.println("Welcome to BlackJack!");
    user = new User();
    dealer = new Dealer();
    do {
      showBank();
      System.out.println("Enter your bet");
      setBet(input.nextInt());
      game.play();
      changeBank();
      game.resetPlayers();
      if (user.getBank() <= 0 || dealer.getBank() <= 0) {
        break;
      }
      System.out.println("Continue?\n1-Yes\n2-No");
      if (input.nextInt() == 2) {
        break;
      }
      System.out.println("============================");
    } while (true);
    if (user.getBank() > dealer.getBank()) {
      System.out.println("You won $" + (user.getBank() - dealer.getBank()) / 2);
    } else {
      System.out.println("You lost $" + (dealer.getBank() - user.getBank()) / 2);
    }
  }

  @Override
  public void showBank() {
    System.out.println("Your bank: " + user.getBank() + "$");
    System.out.println("Dealer's bank " + dealer.getBank() + "$");
  }

  @Override
  public void changeBank() {
    if (game.getUserIsWon()) {
      user.setBank(user.getBank() + bet);
      dealer.setBank(dealer.getBank() - bet);
    } else {
      dealer.setBank(dealer.getBank() + bet);
      user.setBank(user.getBank() - bet);
    }
    showBank();
  }

  @Override
  public void setBet(final int currentBet) {
    if (currentBet > user.getBank()) {
      bet = user.getBank();
    } else if (currentBet > dealer.getBank()) {
      bet = dealer.getBank();
    } else {
      bet = currentBet;
    }
  }
}



