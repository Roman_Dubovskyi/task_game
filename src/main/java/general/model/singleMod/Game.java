package general.model.singleMod;

import general.model.deck.Card;
import general.model.deck.Deck;
import general.model.player.Player;

import java.util.Collections;
import java.util.Scanner;

/**
 * This class creates logic for one cards distribution.
 */
public class Game {
  private Deck deck;
  private Player dealer = new Dealer();
  private Player user = new User();
  private static final int FIRST_CARD = 0;
  private static final int BLACK_JACK = 21;
  private static final int ACE_ELEVEN = 11;
  private static final int ACE_DIFF = 10;
  private static final int ACE_ONE = 1;
  private static final int DEALER_STOP = 16;
  private Scanner input = new Scanner(System.in, "UTF-8");


  private void startOfGame() {
    deck = new Deck();
    dealer.setName("Dealer:");
    user.setName("User:");
    Collections.shuffle(deck.getAllDeck());
    hit(user);
    hit(dealer);
    dealer.printCards();
    System.out.println("dark card");
    hit(user);
    user.printCards();
    hit(dealer);

  }

  private void hit(final Player player) {
    player.getCurrentCards().add(deck.getOneCard(FIRST_CARD));
    int getCardsHit = player.getCurrentCards().get(player.getCurrentCards().size() - 1).getValue();
    player.setCardsValue(player.getCardsValue() + getCardsHit);
  }

  private void checkBust(final Player player) {
    if (player.getCardsValue() > BLACK_JACK) {
      for (Card card : player.getCurrentCards()) {
        if (card.getValue() == ACE_ELEVEN) {
          card.setValue(ACE_ONE);
          player.setCardsValue(player.getCardsValue() - ACE_DIFF);
        }
      }
      if (player.getCardsValue() > BLACK_JACK) {
        System.out.println(player.getName() + "(Bust)");
        player.printCards();
        player.setWon(false);
      }
    }
  }

  private void printPlayersCards() {
    user.printCards();
    dealer.printCards();
  }

  private void compareValues() {
    if (user.getCardsValue() > dealer.getCardsValue()) {
      System.out.println(user.getName() + " won");
      dealer.setWon(false);
      printPlayersCards();
    } else if (user.getCardsValue() == dealer.getCardsValue()) {
      if (user.getCardsValue() == BLACK_JACK) {
        System.out.println(user.getName() + " won");
        dealer.setWon(false);
        printPlayersCards();
      } else {
        System.out.println(dealer.getName() + " won");
        user.setWon(false);
        printPlayersCards();
      }
    } else {
      System.out.println(dealer.getName() + " won");
      user.setWon(false);
      printPlayersCards();
    }
  }

  private void hitUser() {
    do {
      hit(user);
      checkBust(user);
      if (user.getIsWon()) {
        user.printCards();
        System.out.println("1-Hit\n2-Stand");
        if (!input.nextLine().equals("1")) {
          break;
        }
      }
    } while (user.getIsWon());
  }

  private void hitDealer() {
    while (dealer.getCardsValue() <= DEALER_STOP) {
      hit(dealer);
      checkBust(dealer);
    }
  }

  public void play() {
    startOfGame();
    System.out.println("1-Hit\n2-Stand");
    if (input.nextLine().equals("1")) {
      hitUser();
    }
    if (user.getIsWon()) {
      hitDealer();
      if (dealer.getIsWon()) {
        compareValues();
      }
    }
    System.out.println("UserIsWon: " + user.getIsWon());
    System.out.println("DealerIsWon: " + dealer.getIsWon());

  }

  public void resetPlayers() {
    user.getCurrentCards().removeAll(user.getCurrentCards());
    dealer.getCurrentCards().removeAll(dealer.getCurrentCards());
    user.setCardsValue(0);
    dealer.setCardsValue(0);
    user.setWon(true);
    dealer.setWon(true);
  }

  public boolean getUserIsWon() {
    return user.getIsWon();
  }
}
