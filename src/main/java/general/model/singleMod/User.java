package general.model.singleMod;

import general.model.player.Player;

/**
 * This class sets parameters fot user in single mode.
 */

public class User extends Player {
  private static final int USER_START_BANK = 500;

  public User() {
    super();
    setName("You");
    setBank(USER_START_BANK);
  }
}
