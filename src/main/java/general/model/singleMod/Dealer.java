package general.model.singleMod;

import general.model.player.Player;

/**
 * This class sets parameters for Dealer.
 */

public class Dealer extends Player {
  private static final int DEALER_START_BANK = 500;

  public Dealer() {
    super();
    setName("Dealer");
    setBank(DEALER_START_BANK);
  }
}
