package general.model;

public interface ModelOfGame {

  /**
   * This method conducts series of games in BlackJack.
   */
  void playSeries();

  /**
   * This method prints users bank.
   */
  void showBank();

  /**
   * This method changes players bank after one game.
   */
  void changeBank();

  /**
   * This method allows user to enter a bet in the game.
   * Also puts restriction on the bet depending on current bank
   * of players.
   * @param bet takes size of bet to set
   */
  void setBet(int bet);
}
