package general.model;

public interface ModelOfTournament {
  /**
   * This is main method, that conducts Tournament.
   */
  void playTournament();

  /**
   * This method prints the welcome message.
   */
  void welcomeUserToT();

  /**
   * This method sets configuration of round in
   * tournament.
   */
  void setRoundScore();

  /**
   * This method implements logic of user choice in the
   * end of a round.
   */
  void choice();

  /**
   * This method resets round counter variable after
   * tournament is finished.
   */
  void resetRoundCounter();
}
