package general.model.deck;

/**
 * This class creates Club suit.
 */
class Club extends Card {
  Club(String name, int value) {
    super(name, value);
    this.suit = Suit.CLUB;
  }
}
