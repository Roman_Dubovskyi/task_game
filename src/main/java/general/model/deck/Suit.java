package general.model.deck;

/**
 * This enum is used to create suit for cards.
 */
public enum Suit {
  CLUB("♣"),
  DIAMOND("♦"),
  HEART("♥"),
  SPADE("♠");

  public String getShape() {
    return shape;
  }

  private String shape;


   Suit(String shape) {
    this.shape = shape;
  }

}
