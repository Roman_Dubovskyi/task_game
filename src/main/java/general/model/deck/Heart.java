package general.model.deck;

/**
 * This class creates Heart suit.
 */
class Heart extends Card {
  Heart(String name, int value) {
    super(name, value);
    this.suit = Suit.HEART;
  }

}
