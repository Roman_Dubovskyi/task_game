package general.model.deck;

/**
 * This class creates Spade suit.
 */
class Spade extends Card {
  Spade(String name, int value) {
    super(name, value);
    this.suit = Suit.SPADE;
  }

}
