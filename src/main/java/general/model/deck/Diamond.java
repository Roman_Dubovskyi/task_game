package general.model.deck;
/**
 * This class creates Diamond suit.
 */
class Diamond extends Card {
  Diamond(String name, int value) {
    super(name, value);
    this.suit = Suit.DIAMOND;
  }

}
