package general.model.deck;

import java.util.ArrayList;
import java.util.List;

/**
 * This class creates whole deck of cards.
 */
public class Deck {
  private List<Card> deckOfCards;
  private List<Heart> deckOfHeart;
  private List<Spade> deckOfSpade;
  private List<Diamond> deckOfDiamond;
  private List<Club> deckOfClub;


  public Deck() {
    generateDeck();
  }

  private void generateDeck() {
    deckOfCards = new ArrayList<Card>();
    generateHeartDeck();
    generateSpadeDeck();
    generateClubDeck();
    generateDiamondDeck();
    deckOfCards.addAll(deckOfSpade);
    deckOfCards.addAll(deckOfHeart);
    deckOfCards.addAll(deckOfClub);
    deckOfCards.addAll(deckOfDiamond);
  }

  private void generateHeartDeck() {
    deckOfHeart = new ArrayList<Heart>();
    deckOfHeart.add(new Heart("2", 2));
    deckOfHeart.add(new Heart("3", 3));
    deckOfHeart.add(new Heart("4", 4));
    deckOfHeart.add(new Heart("5", 5));
    deckOfHeart.add(new Heart("6", 6));
    deckOfHeart.add(new Heart("7", 7));
    deckOfHeart.add(new Heart("8", 8));
    deckOfHeart.add(new Heart("9", 9));
    deckOfHeart.add(new Heart("10", 10));
    deckOfHeart.add(new Heart("J", 10));
    deckOfHeart.add(new Heart("Q", 10));
    deckOfHeart.add(new Heart("K", 10));
    deckOfHeart.add(new Heart("A", 11));

  }

  private void generateClubDeck() {
    deckOfClub = new ArrayList<Club>();
    deckOfClub.add(new Club("2", 2));
    deckOfClub.add(new Club("3", 3));
    deckOfClub.add(new Club("4", 4));
    deckOfClub.add(new Club("5", 5));
    deckOfClub.add(new Club("6", 6));
    deckOfClub.add(new Club("7", 7));
    deckOfClub.add(new Club("8", 8));
    deckOfClub.add(new Club("9", 9));
    deckOfClub.add(new Club("10", 10));
    deckOfClub.add(new Club("J", 10));
    deckOfClub.add(new Club("Q", 10));
    deckOfClub.add(new Club("K", 10));
    deckOfClub.add(new Club("A", 11));

  }

  private void generateSpadeDeck() {
    deckOfSpade = new ArrayList<Spade>();
    deckOfSpade.add(new Spade("2", 2));
    deckOfSpade.add(new Spade("3", 3));
    deckOfSpade.add(new Spade("4", 4));
    deckOfSpade.add(new Spade("5", 5));
    deckOfSpade.add(new Spade("6", 6));
    deckOfSpade.add(new Spade("7", 7));
    deckOfSpade.add(new Spade("8", 8));
    deckOfSpade.add(new Spade("9", 9));
    deckOfSpade.add(new Spade("10", 10));
    deckOfSpade.add(new Spade("J", 10));
    deckOfSpade.add(new Spade("Q", 10));
    deckOfSpade.add(new Spade("K", 10));
    deckOfSpade.add(new Spade("A", 11));

  }

  private void generateDiamondDeck() {
    deckOfDiamond = new ArrayList<Diamond>();
    deckOfDiamond.add(new Diamond("2", 2));
    deckOfDiamond.add(new Diamond("3", 3));
    deckOfDiamond.add(new Diamond("4", 4));
    deckOfDiamond.add(new Diamond("5", 5));
    deckOfDiamond.add(new Diamond("6", 6));
    deckOfDiamond.add(new Diamond("7", 7));
    deckOfDiamond.add(new Diamond("8", 8));
    deckOfDiamond.add(new Diamond("9", 9));
    deckOfDiamond.add(new Diamond("10", 10));
    deckOfDiamond.add(new Diamond("J", 10));
    deckOfDiamond.add(new Diamond("Q", 10));
    deckOfDiamond.add(new Diamond("K", 10));
    deckOfDiamond.add(new Diamond("A", 11));

  }


  public final List<Card> getAllDeck() {
    return deckOfCards;
  }

  public final Card getOneCard(int index) {
    Card card = deckOfCards.get(index);
    deckOfCards.remove(index);
    return card;
  }

  public final Card removeOneCard(int index) {
    return deckOfCards.remove(index);
  }

  public List<Card> getDeckOfCards() {
    return deckOfCards;
  }
}
