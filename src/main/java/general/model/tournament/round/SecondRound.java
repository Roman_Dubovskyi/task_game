package general.model.tournament.round;

import general.model.tournament.Round;

public class SecondRound extends Round {
  private static final int USER_START_SCORE = 200;
  private static final int DEALER_START_SCORE = 200;
  private static final int SECOND_ROUND_REWARD = 500;
  public SecondRound(){
    setDealerStartScore(DEALER_START_SCORE);
    setStartUserScore(USER_START_SCORE);
    setReward(SECOND_ROUND_REWARD);
  }
}
