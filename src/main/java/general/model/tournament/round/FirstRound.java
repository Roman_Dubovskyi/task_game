package general.model.tournament.round;

import general.model.tournament.Round;

public class FirstRound extends Round {
  private static final int USER_START_SCORE = 500;
  private static final int DEALER_START_SCORE = 200;
  private static final int FIRST_ROUND_REWARD = 200;

  public FirstRound(){
    setStartUserScore(USER_START_SCORE);
    setDealerStartScore(DEALER_START_SCORE);
    setReward(FIRST_ROUND_REWARD);
  }
}
