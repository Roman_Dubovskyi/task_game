package general.model.tournament.round;

import general.model.tournament.Round;

public class ThirdRound extends Round {
  private static final int USER_START_SCORE = 200;
  private static final int DEALER_START_SCORE = 500;
  private static final int THIRD_ROUND_REWARD = 1000;
  public ThirdRound(){
    setDealerStartScore(DEALER_START_SCORE);
    setStartUserScore(USER_START_SCORE);
    setReward(THIRD_ROUND_REWARD);
  }
}
