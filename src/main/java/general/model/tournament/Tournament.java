package general.model.tournament;

import general.model.ModelOfTournament;
import general.model.player.PlayerT;
import general.model.tournament.round.FirstRound;
import general.model.tournament.round.SecondRound;
import general.model.tournament.round.ThirdRound;

import java.util.Scanner;

/**
 * Class implements logic of whole tournament.
 */
public class Tournament implements ModelOfTournament {
  private int nOfRounds = 3;
  SingleRound sRound = new SingleRound();
  Round firstRound = new FirstRound();
  Round secondRound = new SecondRound();
  Round thirdRound = new ThirdRound();
  PlayerT usert = new UserT();
  private int roundCounter = 1;
  private int roundReward;
  private Scanner input = new Scanner(System.in, "UTF-8");

  @Override
  public void playTournament() {
    welcomeUserToT();
    resetRoundCounter();
    do {
      setRoundScore();
      System.out.println("Round: " + roundCounter);
      System.out.println("Round reward is: " + roundReward + "$");
      sRound.playSeries();
      choice();
    } while (usert.isToNextRound() && roundCounter <= nOfRounds);

  }

  @Override
  public void welcomeUserToT() {
    System.out.println("Tournament start.");
  }

  @Override
  public void setRoundScore() {
    usert.setToNextRound(false);
    switch (roundCounter) {
      case 1:
        sRound.userT.setScore(firstRound.getUserStartScore());
        sRound.dealerT.setScore(firstRound.getDealerStartScore());
        roundReward = firstRound.getReward();
        break;
      case 2:
        sRound.userT.setScore(secondRound.getUserStartScore());
        sRound.dealerT.setScore(secondRound.getDealerStartScore());
        roundReward = secondRound.getReward();
        break;
      case 3:
        sRound.userT.setScore(thirdRound.getUserStartScore());
        sRound.dealerT.setScore(thirdRound.getDealerStartScore());
        roundReward = thirdRound.getReward();
        break;
    }
  }

  @Override
  public void choice() {
    if (sRound.userT.getScore() > sRound.dealerT.getScore()) {
      if (roundCounter == nOfRounds) {
        System.out.println("Congratulations, you won highest prize");
      }
      System.out.println("You won $" + roundReward + " in round " + roundCounter);
      System.out.println("1- Next round\n2- Get reward");

      if (input.nextInt() == 1) {
        roundCounter++;
        usert.setToNextRound(true);
      } else {
        System.out.println("Game over");
      }
    } else {
      System.out.println("You lost in round " + roundCounter);

    }
  }

  @Override
  public void resetRoundCounter() {
    roundCounter = 1;
  }

}
