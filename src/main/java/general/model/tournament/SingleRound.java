package general.model.tournament;

import general.model.ModelOfGame;
import general.model.player.PlayerT;
import general.model.singleMod.Game;

import java.util.Scanner;

/**
 * This class implements logic of series of game in Blackjack
 * with the one bank.
 */
public class SingleRound implements ModelOfGame {
  private Game game = new Game();
  public PlayerT userT = new UserT();
  public PlayerT dealerT = new DealerT();
  private Scanner input = new Scanner(System.in, "UTF-8");
  private int bet;

  @Override
  public void playSeries() {
    showBank();
    do {
      System.out.println("Enter your bet");
      setBet(input.nextInt());
      game.play();
      changeBank();
      game.resetPlayers();
      if (userT.getScore() <= 0 || dealerT.getScore() <= 0) {
        break;
      }
    } while (true);
  }

  @Override
  public void changeBank() {
    if (game.getUserIsWon()) {
      userT.setScore(userT.getScore() + bet);
      dealerT.setScore(dealerT.getScore() - bet);
    } else {
      dealerT.setScore(dealerT.getScore() + bet);
      userT.setScore(userT.getScore() - bet);
    }
    showBank();
  }

  @Override
  public void setBet(final int currentBet) {
    if (currentBet > userT.getScore()) {
      bet = userT.getScore();
    } else if (currentBet > dealerT.getScore()) {
      bet = dealerT.getScore();
    } else {
      bet = currentBet;
    }
  }

  @Override
  public void showBank() {
    System.out.print("User's score:");
    System.out.println(userT.getScore());
    System.out.print("Dealer's score:");
    System.out.println(dealerT.getScore());
  }
}
