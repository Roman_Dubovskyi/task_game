package general.model.tournament;

import general.model.player.PlayerT;

/**
 * This class sets parameters of dealer in tournament.
 */
public class DealerT extends PlayerT {

  public DealerT() {
    super();
    setName("Dealer");
  }
}
