package general.model.tournament;

/**
 * This class sets parameters for single round.
 */
public abstract class Round {
  private int userStartScore;
  private int dealerStartScore;
  private int reward;

  public int getUserStartScore() {
    return userStartScore;
  }

  public void setStartUserScore(final int userStartScore) {
    this.userStartScore = userStartScore;
  }

  public int getDealerStartScore() {
    return dealerStartScore;
  }

  public void setDealerStartScore(final int dealerStartScore) {
    this.dealerStartScore = dealerStartScore;
  }


  public int getReward() {
    return reward;
  }

  public void setReward(final int reward) {
    this.reward = reward;
  }

}
