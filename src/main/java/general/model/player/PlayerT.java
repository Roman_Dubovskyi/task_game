package general.model.player;

/**
 * This class modifies 'Player' class settings for usage
 * in tournament mode.
 */
public abstract class PlayerT extends Player {
  private int score;
  private boolean toNextRound;

  public void setScore(int score) {
    this.score = score;
  }

  public int getScore() {
    return score;
  }

  public boolean isToNextRound() {
    return toNextRound;
  }

  public void setToNextRound(boolean toNextRound) {
    this.toNextRound = toNextRound;
  }
}
