package general.model.player;

import general.model.deck.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * This class creates a model of player that is used
 * on single mode game.
 */
public abstract class Player {
  private String name;
  private List<Card> currentCards = new ArrayList<Card>();
  private int cardsValue = 0;
  private boolean isWon = true;
  private int bank;

  public int getBank() {
    return bank;
  }

  public void setBank(int bank) {
    this.bank = bank;
  }


  public boolean getIsWon() {
    return isWon;
  }

  public void setWon(boolean won) {
    isWon = won;
  }

  public List<Card> getCurrentCards() {
    return currentCards;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCardsValue() {
    return cardsValue;
  }

  public void setCardsValue(int cardsValue) {
    this.cardsValue = cardsValue;
  }

  public void printCards() {
    System.out.println(this.name);
    for (Card card : currentCards) {
      System.out.print(card.toString() + "\n");
    }


  }
}
