package general;

import general.view.MyView;

/**
 * This is main class of the program.
 *
 * @author Roman Dubovskyi
 * @version 1.0
 */
public final class Application {
  private Application() {
  }

  /**
   * Creating variable to call 'show' method
   * from 'MyView'.
   */
  private static MyView view = new MyView();

  /**
   * This is main method of the program.
   * @param args some arguments
   */
  public static void main(final String[] args) {
    view.show();
  }
}

