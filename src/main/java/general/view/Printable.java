package general.view;

public interface Printable {
  void print();
}
