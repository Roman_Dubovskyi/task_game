package general.view;

import general.controller.ControllerImp;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This class is used to create visual part
 * of program.
 */

public class MyView {
  private ControllerImp controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in, "UTF-8");

  /**
   * This method creates two maps,
   * which will contain options in menu for user
   * and their content. That will be used to create a menu.
   */

  public MyView() {
    controller = new ControllerImp();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Start single game mode");
    menu.put("2", "  2 - Start tournament game mode");
    menu.put("3", "  3 - View single game rules");
    menu.put("4", "  4 - View tournament rules");
    menu.put("Q", "  Q - Exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
  }

  private void pressButton1() {
    controller.executeSingleGame();
  }

  private void pressButton2() {
    controller.makeTournament();
  }

  private void pressButton3() {
    controller.printSingleGameRules();
  }

  private void pressButton4() {
    controller.printTournamentRules();
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  /**
   * This method prints menu, when game starts.
   */

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
